package edu.pja.sri.mdziurzynski.s22012.rest;

import edu.pja.sri.mdziurzynski.s22012.dto.EmployeeDto;
import edu.pja.sri.mdziurzynski.s22012.model.Employee;
import edu.pja.sri.mdziurzynski.s22012.repo.EmployeeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {
    private EmployeeRepository employeeRepository;
    private ModelMapper modelMapper;

    public EmployeeController(EmployeeRepository employeeRepository, ModelMapper modelMapper) {
        this.employeeRepository = employeeRepository;
        this.modelMapper = modelMapper;
    }

    private EmployeeDto convertToDto(Employee employee) {
        return modelMapper.map(employee, EmployeeDto.class);
    }

    private Employee convertToEntity(EmployeeDto employeeDto) {
        return modelMapper.map(employeeDto, Employee.class);
    }

    @GetMapping
    public ResponseEntity<Collection<EmployeeDto>> getEmployees() {
        List<Employee> allEmployees = employeeRepository.findAll();
        List<EmployeeDto> result = allEmployees.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/{empId}")
    public ResponseEntity<EmployeeDto> getEmployeeById(@PathVariable Long empId) {
        Optional<Employee> employee = employeeRepository.findById(empId);
        if (employee.isPresent()) {
            EmployeeDto employeeDto = convertToDto(employee.get());
            return new ResponseEntity<>(employeeDto, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity addEmployee(@RequestBody EmployeeDto employeeDto) {
        Employee employeeEntity = convertToEntity(employeeDto);
        employeeRepository.save(employeeEntity);

        HttpHeaders headers = new HttpHeaders();
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(employeeEntity.getId())
                .toUri();
        headers.add("Location", location.toString());

        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @PutMapping("/{empId}")
    public ResponseEntity updateEmployee(@PathVariable Long empId, @RequestBody EmployeeDto employeeDto) {
        Optional<Employee> currentEmployee = employeeRepository.findById(empId);
        if (currentEmployee.isPresent()) {
            employeeDto.setId(empId);
            Employee employeeEntity = convertToEntity(employeeDto);
            employeeRepository.save(employeeEntity);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{empId}")
    public ResponseEntity deleteEmployee(@PathVariable Long empId) {
        employeeRepository.deleteById(empId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
